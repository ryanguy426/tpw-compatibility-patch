﻿Public Class Welcome
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If IsVistaOrHigher() Then
            If IsAdmin() Then
            Else
                RestartElevated()
            End If
        Else
            MsgBox("OS too old!")
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles CancelButton.Click
        Application.Exit()
    End Sub

    Private Sub ProceedButton_Click(sender As Object, e As EventArgs) Handles ProceedButton.Click
        Dim result As Integer = MessageBox.Show("Do you have Gold Edition installed?", "Question #1", MessageBoxButtons.YesNo)
        If result = DialogResult.No Then
            Dim result2 As Integer = MessageBox.Show("Do you have the 2.0 patch installed?", "Question #2", MessageBoxButtons.YesNo)
            If result2 = DialogResult.No Then
                MessageBox.Show("Go do that.", "That's a problem")
                Application.Exit()
            ElseIf result2 = DialogResult.Yes Then
                Me.Hide()
                Download.ShowDialog()
            End If
        ElseIf result = DialogResult.Yes Then
            Me.Hide()
            Download.ShowDialog()
        End If
    End Sub
End Class
