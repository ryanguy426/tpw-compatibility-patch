﻿Imports System.ComponentModel
Imports System.Net
Imports System.IO
Imports System.IO.Compression
Public Class Download
    Private Sub Download_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Application.Exit()
    End Sub

    Private Sub Download_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label4.Text = My.Computer.Info.OSFullName
        If My.Computer.FileSystem.DirectoryExists(My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\TPpatch") Then
            Check_OS()
        Else
            My.Computer.FileSystem.CreateDirectory((My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\TPpatch"))
			My.Computer.FileSystem.CreateDirectory((My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\TPpatch\Extract"))
            Check_OS()
        End If
    End Sub

    Private Sub Check_OS()
        If Label4.Text.Contains("Vista") Then
            If My.Computer.FileSystem.FileExists((My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\TPpatch.Patch.zip")) Then
                BackgroundWorker1.RunWorkerAsync()
            Else
                Vista7_download()
            End If
        ElseIf Label4.Text.Contains("7") Then
            If My.Computer.FileSystem.FileExists((My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\TPpatch.Patch.zip")) Then
                BackgroundWorker1.RunWorkerAsync()
            Else
                Vista7_download()
            End If
        ElseIf Label4.Text.Contains("8") Then
            If My.Computer.FileSystem.FileExists((My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\TPpatch\Patch.zip")) Then
                BackgroundWorker1.RunWorkerAsync()
            Else
                Win8_10_download()
            End If
        ElseIf Label4.Text.Contains("10") Then
            If My.Computer.FileSystem.FileExists((My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\TPpatch\Patch.zip")) Then
                BackgroundWorker1.RunWorkerAsync()
            Else
                Win8_10_download()
            End If
        End If
    End Sub

    Private Sub Vista7_download()
        Dim client As WebClient = New WebClient
        AddHandler client.DownloadProgressChanged, AddressOf client_ProgressChanged
        AddHandler client.DownloadFileCompleted, AddressOf client_DownloadCompleted
        client.DownloadFileAsync(New Uri("http://download1274.mediafire.com/6eoapfi0sq1g/kf9zxz42eb7aors/Vista-7+patch.zip"), My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\TPpatch\Patch.zip")
    End Sub

    Private Sub client_DownloadCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        Label2.Text = "Applying patch"
        ProgressBar1.Value = 0
        ProgressBar1.Style = ProgressBarStyle.Marquee
        MsgBox("Start Async")
        BackgroundWorker1.RunWorkerAsync()
    End Sub

    Private Sub Win8_10_download()
        MsgBox("Start download")
        Dim client As WebClient = New WebClient
        AddHandler client.DownloadProgressChanged, AddressOf client_ProgressChanged
        AddHandler client.DownloadFileCompleted, AddressOf client_DownloadCompleted
        client.DownloadFileAsync(New Uri("http://download854.mediafire.com/ut8xwclscqig/u766npr17fgr05q/8-10+patch.zip"), My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\TPpatch\Patch.zip")
    End Sub

    Private Sub client_ProgressChanged(ByVal sender As Object, ByVal e As DownloadProgressChangedEventArgs)
        Dim bytesIn As Double = Double.Parse(e.BytesReceived.ToString())
        Dim totalBytes As Double = Double.Parse(e.TotalBytesToReceive.ToString())
        Dim percentage As Double = bytesIn / totalBytes * 100
        ProgressBar1.Value = Int32.Parse(Math.Truncate(percentage).ToString())
    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim zipPath As String = My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\TPpatch\Patch.zip"
        Dim extractPath As String = Environment.GetEnvironmentVariable("PROGRAMFILES") & "\SimTheme Park\"
        'ZipFile.ExtractToDirectory(zipPath, extractPath)
        Using archive As ZipArchive = ZipFile.OpenRead(zipPath)
            For Each entry As ZipArchiveEntry In archive.Entries
                entry.ExtractToFile(Path.Combine(extractPath, entry.FullName), True)
            Next
        End Using
        MsgBox("Done extracting")
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        ProgressBar1.Value = ProgressBar1.Value + 1
    End Sub

	Private sub ExtractToTemp
		Dim zipPath As String = My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\TPpatch\Patch.zip"
        Dim extractPath As String = My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\TPpatch\Extract\"
        ZipFile.ExtractToDirectory(zipPath, extractPath)
		
	End sub
End Class