﻿Module RegPatch
    Public Sub PatchReg()
        'Declare the process we will use to patch
        Dim p As Process = New Process
        Dim ps As ProcessStartInfo = New ProcessStartInfo
        'Processstartinfo.filename to start regedit
        ps.FileName = "regedit"
        'Start regedit with "/s" so it's silent and "\tp.reg" to apply the registry tweak needed to run the game
        ps.Arguments = " /s " & Application.StartupPath + "\tp.reg"
        p.StartInfo = ps
        'Start the regedit process
        Try
            p.Start()
        Catch ex As Exception
            MsgBox("Well crap. The registry couldn't be modified :/")
        End Try
    End Sub
End Module
